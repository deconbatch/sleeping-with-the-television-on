/**
 * Sleeping with the Television On.
 * 
 * @author @deconbatch
 * @version 0.4
 * Processing 3.2.1
 * created 2018.06.03
 *  Some noise pattern that I saw on old analog television.
 * updated 2019.02.09
 *  smooth animation.
 * updated 2019.07.20
 *  blendMode(SCREEN) -> DIFFERENCE
 *  loop animation with sin(PI) -> I realized I don't need loop animation with this
 * updated 2019.09.20
 *  make the pattern changing speed slow
 *  adde a television tube screen frame
 */

void setup() {

  size(720, 540); // 4:3
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  smooth();
  noLoop();
  noStroke();

}

void draw() {

  int   frameCntMax = 15 * 8;
  int   spotCycle   = 10;
  float rWidth      = width * 0.5;
  float rHeight     = height * 0.5;
  float hueBase     = random(360);

  translate(width / 2, height / 2);
  for (int frames = 0; frames < frameCntMax; ++frames) {

    background(0.0, 0.0, 90.0, 100.0);
    
    float frmRatio  = map(frames, 0, frameCntMax, 0.0, 1.0); //easeInOutCubic(map(frames, 0, frameCntMax, 0.0, 1.0));
    float easeRatio = easeInOutCubic(frmRatio);

    // cyclic random noise pattern
    float pattern = noise(10.0 + cos(TWO_PI * frmRatio) * 0.01, 10.0 + sin(TWO_PI * frmRatio) * 0.01) * 0.1;

    // draw noise
    blendMode(DIFFERENCE);
    noStroke();
    for (int spotCnt = 1; spotCnt <= 50000; ++spotCnt) {

      float radian = pattern * spotCnt;
      float x = rWidth * cos(radian * sin(pattern));
      float y = rHeight * sin(radian + PI * spotCnt * 0.000005 * sin(PI * easeRatio));

      float eSiz = map(spotCnt % spotCycle, 0, spotCycle, 0.0, 30.0);
      float eBri = map(spotCnt % spotCycle, 0, spotCycle, 20.0, 0.0);
      float eHue = hueBase + sin(radian) * 60.0;

      fill(eHue % 360.0, 40.0, eBri, 100.0);
      ellipse(x, y, eSiz, eSiz);
      
    }

    // draw television screen frame
    blendMode(BLEND);
    strokeWeight(50.0);
    stroke((hueBase + 180.0) % 360.0, 90.0, 5.0, 100.0);
    noFill();
    rect(0.0, 0.0, width, height);
    rect(0.0, 0.0, width, height, 180.0);

    saveFrame("frames/" + String.format("%04d", frames) + ".png");

  }

  exit();

}

/**
 * easeInOutCubic easing function.
 * @param  t     0.0 - 1.0 : linear value.
 * @return float 0.0 - 1.0 : eased value.
 */
float easeInOutCubic(float t) {
  t *= 2.0;
  if (t < 1.0) {
    return pow(t, 3) / 2.0;
  }
  t -= 2.0;
  return (pow(t, 3) + 2.0) / 2.0;
}
